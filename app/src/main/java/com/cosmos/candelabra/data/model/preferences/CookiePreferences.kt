package com.cosmos.candelabra.data.model.preferences

import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey

data class CookiePreferences(
    val crumb: String?,
    val created: Long?
) {
    object PreferenceKeys {
        val CRUMB = stringPreferencesKey("crumb")
        val CREATED = longPreferencesKey("created")
    }
}
