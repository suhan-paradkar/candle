package com.cosmos.candelabra.data.remote.api.yahoofinance

import com.cosmos.candelabra.data.local.dao.CookieDao
import com.cosmos.candelabra.data.local.mapper.CookieMapper
import com.cosmos.candelabra.di.CoroutinesScopesModule.ApplicationScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class YahooCookieJar @Inject constructor(
    private val cookieMapper: CookieMapper,
    private val cookieDao: CookieDao,
    @ApplicationScope private val coroutineScope: CoroutineScope
) : CookieJar {

    private val cookies: MutableMap<String, Cookie> = mutableMapOf()

    val isEmpty: Boolean
        get() = cookies.isEmpty()

    val size: Int
        get() = cookies.size

    suspend fun initCookies() {
        cookieDao
            .getAllCookies()
            .map { cookieMapper.dataFromEntity(it) }
            .forEach { cookies[it.name] = it }
    }

    suspend fun emptyJar() {
        cookies.clear()
        cookieDao.deleteAllCookies()
    }

    override fun loadForRequest(url: HttpUrl): List<Cookie> {
        return cookies.values.toList()
    }

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        val toPersist = mutableListOf<Cookie>()

        cookies.forEach { cookie ->
            val previous = this.cookies.put(cookie.name, cookie)

            // Persist if the cookie was not already present in the map
            if (previous == null) toPersist.add(cookie)
        }

        coroutineScope.launch { persistCookies(toPersist) }
    }

    private suspend fun persistCookies(cookies: List<Cookie>) {
        cookieMapper
            .dataToEntities(cookies)
            .forEach { cookieDao.upsert(it) }
    }
}
