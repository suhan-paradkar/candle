package com.cosmos.candelabra.data.local.mapper

import com.cosmos.candelabra.data.model.Chart
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.ChartResult
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.Charts
import com.cosmos.candelabra.di.DispatchersModule.DefaultDispatcher
import com.github.mikephil.charting.data.Entry
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ChartMapper @Inject constructor(
    @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
) : Mapper<ChartResult, Chart>(defaultDispatcher) {

    override suspend fun toEntity(from: ChartResult): Chart {
        return with(from) {
            Chart(
                meta.symbol,
                meta.currency,
                meta.regularMarketPrice,
                meta.previousClose,
                mapDatesWithPrices(timestamp, indicators.quote.firstOrNull()?.close ?: emptyList())
            )
        }
    }

    suspend fun dataToEntity(data: Charts): Chart? {
        data.chart.result.firstOrNull()?.let {
            return toEntity(it)
        }
        return null
    }

    private fun mapDatesWithPrices(
        timestamps: List<Long>,
        prices: List<Double?>
    ): List<Entry> {
        return timestamps.zip(prices).mapNotNull { pair ->
            pair.second?.let { price ->
                Entry(
                    pair.first.times(1000).toFloat(), // Convert timestamps to milliseconds
                    price.toFloat()
                )
            }
        }
    }
}
