package com.cosmos.candelabra.data.repository

import androidx.appcompat.app.AppCompatDelegate
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import com.cosmos.candelabra.data.model.preferences.CookiePreferences
import com.cosmos.candelabra.data.model.preferences.DataPreferences
import com.cosmos.candelabra.data.model.preferences.UiPreferences
import com.cosmos.candelabra.util.extension.getValue
import com.cosmos.candelabra.util.extension.setValue
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PreferencesRepository @Inject constructor(
    private val preferencesDatastore: DataStore<Preferences>
) {

    //region Ui

    suspend fun setNightMode(nightMode: Int) {
        preferencesDatastore.setValue(UiPreferences.PreferencesKeys.NIGHT_MODE, nightMode)
    }

    fun getNightMode(defaultValue: Int = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM): Flow<Int> {
        return preferencesDatastore.getValue(UiPreferences.PreferencesKeys.NIGHT_MODE, defaultValue)
    }

    //endregion

    //region Data

    suspend fun setQuotesLastRefresh(lastRefresh: Long) {
        preferencesDatastore.setValue(
            DataPreferences.PreferenceKeys.QUOTES_LAST_REFRESH,
            lastRefresh
        )
    }

    fun getQuotesLastRefresh(defaultValue: Long = 0): Flow<Long> {
        return preferencesDatastore.getValue(
            DataPreferences.PreferenceKeys.QUOTES_LAST_REFRESH,
            defaultValue
        )
    }

    suspend fun setChartLastRefresh(lastRefresh: Long) {
        preferencesDatastore.setValue(
            DataPreferences.PreferenceKeys.CHART_LAST_REFRESH,
            lastRefresh
        )
    }

    fun getChartLastRefresh(defaultValue: Long = 0): Flow<Long> {
        return preferencesDatastore.getValue(
            DataPreferences.PreferenceKeys.CHART_LAST_REFRESH,
            defaultValue
        )
    }

    suspend fun setChartsLastRefresh(lastRefresh: Long) {
        preferencesDatastore.setValue(
            DataPreferences.PreferenceKeys.CHARTS_LAST_REFRESH,
            lastRefresh
        )
    }

    fun getChartsLastRefresh(defaultValue: Long = 0): Flow<Long> {
        return preferencesDatastore.getValue(
            DataPreferences.PreferenceKeys.CHARTS_LAST_REFRESH,
            defaultValue
        )
    }

    //endregion

    //region Cookie

    suspend fun setCrumb(crumb: String, created: Long = System.currentTimeMillis()) {
        preferencesDatastore.setValue(CookiePreferences.PreferenceKeys.CRUMB, crumb)
        preferencesDatastore.setValue(CookiePreferences.PreferenceKeys.CREATED, created)
    }

    suspend fun removeCrumb() {
        preferencesDatastore.edit { preferences ->
            preferences.remove(CookiePreferences.PreferenceKeys.CRUMB)
            preferences.remove(CookiePreferences.PreferenceKeys.CREATED)
        }
    }

    fun getCookiePreferences(): Flow<CookiePreferences> {
        return preferencesDatastore.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            val crumb = preferences[CookiePreferences.PreferenceKeys.CRUMB]
            val created = preferences[CookiePreferences.PreferenceKeys.CREATED]
            CookiePreferences(crumb, created)
        }
    }

    //endregion
}
