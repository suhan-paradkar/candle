package com.cosmos.candelabra.data.local.mapper

import com.cosmos.candelabra.di.DispatchersModule.DefaultDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.Cookie
import javax.inject.Inject
import javax.inject.Singleton

typealias CookieDb = com.cosmos.candelabra.data.model.db.Cookie

@Singleton
class CookieMapper @Inject constructor(
    @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
) : Mapper<Cookie, CookieDb>(defaultDispatcher) {

    override suspend fun toEntity(from: Cookie): CookieDb {
        return with(from) {
            CookieDb(name, value, expiresAt, domain, path, secure, httpOnly, persistent, hostOnly)
        }
    }

    override suspend fun fromEntity(from: CookieDb): Cookie {
        return with(from) {
            val builder = Cookie.Builder()
                .name(name)
                .value(value)
                .expiresAt(expiresAt)
                .domain(domain)
                .path(path)

            if (secure) {
                builder.secure()
            }

            if (httpOnly) {
                builder.httpOnly()
            }

            builder.build()
        }
    }
}
