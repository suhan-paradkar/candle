package com.cosmos.candelabra.data.remote.api.yahoofinance

import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.ResponseBody
import retrofit2.http.GET

interface YahooFinance {

    @GET("/")
    suspend fun load(): ResponseBody

    companion object {
        val BASE_URL = "https://finance.yahoo.com/".toHttpUrl()
    }
}
