package com.cosmos.candelabra.data.repository

import com.cosmos.candelabra.data.local.StockDatabase
import com.cosmos.candelabra.data.local.mapper.ChartMapper
import com.cosmos.candelabra.data.local.mapper.QuoteMapper
import com.cosmos.candelabra.data.model.Chart
import com.cosmos.candelabra.data.model.ChartPeriod
import com.cosmos.candelabra.data.model.ChartWithQuote
import com.cosmos.candelabra.data.model.MarketState
import com.cosmos.candelabra.data.model.Resource
import com.cosmos.candelabra.data.model.db.Quote
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooFinanceApi
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooInitializer
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.Charts
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.Quotes
import com.cosmos.candelabra.data.remote.api.yahoofinance.model.Search
import com.cosmos.candelabra.di.CoroutinesScopesModule.ApplicationScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.Collections
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@Suppress("TooManyFunctions", "LongParameterList")
class YahooFinanceRepository @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    private val yahooFinanceApi: YahooFinanceApi,
    private val yahooInitializer: YahooInitializer,
    private val stockDatabase: StockDatabase,
    private val quoteMapper: QuoteMapper,
    private val chartMapper: ChartMapper,
    @ApplicationScope private val appScope: CoroutineScope
) : BaseRepository() {

    private suspend fun initializeIfNecessary() {
        if (!yahooInitializer.isInitialized) {
            Timber.d("Yahoo Finance is not initialized")
            yahooInitializer.init()
        }
    }

    private suspend fun getQuoteSync(symbol: String): Resource<Quotes> {
        return getQuotesSync(Collections.singletonList(symbol))
    }

    private suspend fun getQuotesSync(symbols: List<String>): Resource<Quotes> {
        initializeIfNecessary()

        val symbolsQuery = symbols.joinToString(",")
        return safeApiCallSync { yahooFinanceApi.getQuotes(symbolsQuery) }
    }

    fun getQuoteInRealTime(
        symbol: String,
        interval: Long
    ): Flow<Resource<Quote>> = flow {
        var tradeable = false

        do {
            val result = getQuoteSync(symbol)

            when (result) {
                is Resource.Success -> {
                    val quotesResult = result.data!!.quoteResponse.result.firstOrNull()

                    if (quotesResult == null) {
                        emit(Resource.Error())
                        return@flow
                    }

                    val data = quoteMapper.dataToEntity(quotesResult)

                    emit(Resource.Success(data))

                    // Check if at least one quote is tradeable (i.e. market is open)
                    tradeable = (quotesResult.marketState == MarketState.REGULAR.value)
                }

                is Resource.Loading -> emit(Resource.Loading())
                is Resource.Error -> {
                    emit(Resource.Error(result.message, result.code))
                }
            }

            delay(interval)
        } while (result is Resource.Success && tradeable)
    }

    fun getQuotesInRealTime(
        symbols: List<String>,
        interval: Long,
        immediate: Boolean = true
    ): Flow<Resource<List<Quote>>> = flow {
        if (symbols.isEmpty()) {
            emit(Resource.Success(emptyList()))
            return@flow
        }

        var tradeable = false
        var isImmediate = immediate
        var checkTime = true

        do {
            val proceed = shouldProceed(isImmediate, interval, checkTime) {
                preferencesRepository.getQuotesLastRefresh().first()
            }

            if (!proceed) {
                isImmediate = false
                checkTime = false
            }

            val result = getQuotesSync(symbols)

            when (result) {
                is Resource.Success -> {
                    val quotesResult = result.data!!.quoteResponse.result
                    val data = quoteMapper.dataToEntities(quotesResult)

                    appScope.launch {
                        data.forEach {
                            stockDatabase.quoteDao().update(it)
                        }
                    }

                    emit(Resource.Success(data))

                    appScope.launch {
                        preferencesRepository.setQuotesLastRefresh(System.currentTimeMillis())
                    }

                    // Check if at least one quote is tradeable (i.e. market is open)
                    tradeable = quotesResult.any {
                        it.marketState == MarketState.REGULAR.value
                    }
                }

                is Resource.Loading -> emit(Resource.Loading())
                is Resource.Error -> {
                    emit(Resource.Error(result.message, result.code))
                }
            }

            delay(interval)
        } while (result is Resource.Success && tradeable)
    }

    private suspend fun getChartSync(symbol: String, chartPeriod: ChartPeriod): Resource<Charts> {
        return safeApiCallSync {
            yahooFinanceApi.getChart(symbol, chartPeriod.range, chartPeriod.interval)
        }
    }

    fun getChartInRealTime(
        symbol: String,
        chartPeriod: ChartPeriod,
        interval: Long
    ): Flow<Resource<Chart>> = flow {
        var tradeable = false

        do {
            val result = getChartSync(symbol, chartPeriod)

            when (result) {
                is Resource.Success -> {
                    val data = chartMapper.dataToEntity(result.data!!)

                    if (data == null) {
                        emit(Resource.Error())
                        return@flow
                    }

                    emit(Resource.Success(data))

                    tradeable = result.data.chart.result.first().meta.currentTradingPeriod.regular
                        .inPeriod
                }

                is Resource.Loading -> emit(Resource.Loading())
                is Resource.Error -> {
                    emit(Resource.Error(result.message, result.code))
                }
            }

            delay(interval)
        } while (result is Resource.Success && tradeable)
    }

    fun getChartsInRealTime(
        symbols: List<String>,
        chartPeriod: ChartPeriod,
        interval: Long,
        immediate: Boolean = true
    ): Flow<Resource<List<ChartWithQuote>>> = flow {
        if (symbols.isEmpty()) {
            emit(Resource.Success(emptyList()))
            return@flow
        }

        val apiScope = CoroutineScope(SupervisorJob())

        var hasFetchFailed = false
        var tradeable = false
        var isImmediate = immediate
        var checkTime = true

        do {
            val proceed = shouldProceed(isImmediate, interval, checkTime) {
                preferencesRepository.getChartsLastRefresh().first()
            }

            if (!proceed) {
                isImmediate = false
                checkTime = false
            }

            // Fetch data for all quotes
            val quotesDataAsync = apiScope.async { getQuotesSync(symbols) }

            // Fetch chart data for each quote
            val chartsDataAsync = symbols.map { symbol ->
                apiScope.async {
                    val chart = getChartSync(symbol, chartPeriod)
                    mapChart(chart)
                }
            }

            val quotesData = quotesDataAsync.await()
            val chartsData = chartsDataAsync.awaitAll().filterNotNull()

            when (quotesData) {
                is Resource.Success -> {
                    val quotesResult = quotesData.data!!.quoteResponse.result
                    val quoteEntities = quoteMapper.dataToEntities(quotesResult)

                    // Update quotes in database
                    appScope.launch {
                        quoteEntities.forEach {
                            stockDatabase.quoteDao().update(it)
                        }
                    }

                    // Map charts with quotes
                    val newChartsAsync = coroutineScope {
                        chartsData.map { chart ->
                            async(coroutineContext) { mapChart(chart, quoteEntities) }
                        }
                    }

                    // Filter out charts that might not have been successful
                    val newCharts = newChartsAsync.awaitAll().filterNotNull()
                    emit(Resource.Success(newCharts))

                    appScope.launch {
                        preferencesRepository.setChartsLastRefresh(System.currentTimeMillis())
                    }

                    tradeable = quotesResult.any {
                        it.marketState == MarketState.REGULAR.value
                    }
                }

                else -> {
                    hasFetchFailed = true
                    emit(Resource.Error())
                }
            }

            delay(interval)
        } while (!hasFetchFailed && tradeable)
    }

    private fun mapChart(chart: Chart, quotes: List<Quote>): ChartWithQuote? {
        val quote = quotes.find { it.symbol == chart.symbol }
        return quote?.let {
            ChartWithQuote(chart, it)
        }
    }

    private suspend fun mapChart(chart: Resource<Charts>): Chart? {
        return when (chart) {
            is Resource.Success -> {
                return chartMapper.dataToEntity(chart.data!!)
            }

            else -> null
        }
    }

    private suspend fun shouldProceed(
        immediate: Boolean,
        interval: Long,
        checkTime: Boolean,
        lastRefresh: suspend () -> Long
    ): Boolean {
        if (immediate || !checkTime) return true

        val currentTime = System.currentTimeMillis()
        val nextRefreshTime = lastRefresh() + interval

        val timeUntilNextRefresh = nextRefreshTime - currentTime
        delay(timeUntilNextRefresh)

        return false
    }

    fun search(query: String): Flow<Resource<Search>> {
        return safeApiCall { yahooFinanceApi.search(query) }
    }
}
