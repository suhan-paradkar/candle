package com.cosmos.candelabra.ui.watchlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.cosmos.candelabra.R
import com.cosmos.candelabra.data.model.ChartWithQuote
import com.cosmos.candelabra.data.model.Resource
import com.cosmos.candelabra.data.model.db.Quote
import com.cosmos.candelabra.databinding.FragmentWatchlistBinding
import com.cosmos.candelabra.ui.base.BaseFragment
import com.cosmos.candelabra.ui.quotedetails.QuoteDetailsFragmentDirections
import com.cosmos.candelabra.util.extension.latest
import com.cosmos.candelabra.util.extension.launchRepeat
import com.google.android.material.transition.MaterialElevationScale
import com.google.android.material.transition.MaterialFadeThrough
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WatchlistFragment : BaseFragment() {

    private var _binding: FragmentWatchlistBinding? = null
    private val binding get() = _binding!!

    private val viewModel: WatchlistViewModel by activityViewModels()

    private lateinit var watchlistAdapter: WatchlistAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enterTransition = MaterialFadeThrough().apply {
            duration = resources.getInteger(R.integer.motion_duration_large).toLong()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWatchlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }

        initToolbar()
        initRecyclerView()
        bindViewModel()

        binding.infoRetry.setActionClickListener { fetchChartsInRealTime() }
    }

    private fun initToolbar() {
        binding.includeAppbar.toolbar.title = getString(R.string.watchlist_fragment_title)
    }

    private fun initRecyclerView() {
        watchlistAdapter = WatchlistAdapter { cardView, quote ->
            openDetails(cardView, quote)
        }
        binding.listQuotes.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = watchlistAdapter
        }
    }

    private fun bindViewModel() {
        launchRepeat(Lifecycle.State.STARTED) {
            launch {
                viewModel.userCharts.collect()
            }

            launch {
                viewModel.charts.collect { updateCharts(it) }
            }
        }
    }

    private fun fetchChartsInRealTime(forceRefresh: Boolean = true) {
        viewModel.fetchChartsInRealTime(forceRefresh = forceRefresh)
    }

    private fun updateCharts(charts: Resource<List<ChartWithQuote>>) {
        when (charts) {
            is Resource.Success -> {
                watchlistAdapter.submitList(charts.data!!)
            }

            is Resource.Loading -> {
                // TODO
            }

            is Resource.Error -> {
                binding.infoRetry.show()
                viewModel.userCharts.latest?.let { watchlistAdapter.submitList(it) }
            }
        }
    }

    private fun openDetails(cardView: View, quote: Quote) {
        exitTransition = MaterialElevationScale(false).apply {
            duration = resources.getInteger(R.integer.motion_duration_large).toLong()
        }
        reenterTransition = MaterialElevationScale(true).apply {
            duration = resources.getInteger(R.integer.motion_duration_large).toLong()
        }

        val transitionName = getString(R.string.quote_card_detail_transition_name)
        val extras = FragmentNavigatorExtras(cardView to transitionName)
        val directions = QuoteDetailsFragmentDirections.openQuoteDetails(
            quote.symbol,
            quote.name,
            quote
        )

        findNavController().navigate(directions, extras)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
