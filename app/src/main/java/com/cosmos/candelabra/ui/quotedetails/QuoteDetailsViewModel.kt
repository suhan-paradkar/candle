package com.cosmos.candelabra.ui.quotedetails

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cosmos.candelabra.R
import com.cosmos.candelabra.data.model.Chart
import com.cosmos.candelabra.data.model.ChartPeriod
import com.cosmos.candelabra.data.model.QuoteDetail
import com.cosmos.candelabra.data.model.Resource
import com.cosmos.candelabra.data.model.db.Quote
import com.cosmos.candelabra.data.repository.StockRepository
import com.cosmos.candelabra.data.repository.YahooFinanceRepository
import com.cosmos.candelabra.di.DispatchersModule.DefaultDispatcher
import com.cosmos.candelabra.util.CurrencyUtil
import com.cosmos.candelabra.util.extension.format
import com.cosmos.candelabra.util.extension.formatBigNumbers
import com.cosmos.candelabra.util.extension.formatDate
import com.cosmos.candelabra.util.extension.latest
import com.cosmos.candelabra.util.extension.updateValue
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combineTransform
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuoteDetailsViewModel @Inject constructor(
    @ApplicationContext applicationContext: Context,
    private val stockRepository: StockRepository,
    private val yahooFinanceRepository: YahooFinanceRepository,
    @DefaultDispatcher defaultDispatcher: CoroutineDispatcher

) : ViewModel() {

    private var fetchQuoteJob: Job? = null
    private var fetchChartJob: Job? = null

    private val _symbol: MutableStateFlow<String?> = MutableStateFlow(null)

    private val _interval: MutableStateFlow<Long> = MutableStateFlow(DEFAULT_INTERVAL)
    val interval: StateFlow<Long>
        get() = _interval.asStateFlow()

    private val _chartPeriod: MutableStateFlow<ChartPeriod> = MutableStateFlow(DEFAULT_PERIOD)
    val chartPeriod: StateFlow<ChartPeriod>
        get() = _chartPeriod.asStateFlow()

    private val _cachedQuote: MutableStateFlow<Quote?> = MutableStateFlow(null)

    private val _quote: MutableStateFlow<Resource<Quote>> = MutableStateFlow(Resource.Loading())
    val quote: StateFlow<Resource<Quote>>
        get() = _quote.asStateFlow()

    private val _chart: MutableStateFlow<Resource<Chart>> = MutableStateFlow(Resource.Loading())
    val chart: StateFlow<Resource<Chart>>
        get() = _chart.asStateFlow()

    private val quotes: Flow<List<Quote>> = stockRepository.getAllQuotes()

    val isInWatchlist: SharedFlow<Boolean> = combineTransform(_symbol, quotes) { symbol, quotes ->
        symbol?.let { emit(quotes.any { it.symbol.equals(symbol, true) }) }
    }.flowOn(
        defaultDispatcher
    ).shareIn(
        viewModelScope,
        SharingStarted.Eagerly,
        1
    )

    val details: Flow<List<QuoteDetail>> = _cachedQuote.transform { quote ->
        quote?.run {
            val details = mutableListOf<QuoteDetail>()

            open?.let {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_open,
                        CurrencyUtil.formatNumber(it, currency)
                    )
                )
            }

            if (dayLow != null && dayHigh != null) {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_day_range,
                        "${dayLow.format()} - ${dayHigh.format()}"
                    )
                )
            }

            if (ftwLow != null && ftwHigh != null) {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_ftw_range,
                        "${ftwLow.format()} - ${ftwHigh.format()}"
                    )
                )
            }

            volume?.let {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_volume,
                        it.format()
                    )
                )
            }

            markerCap?.let {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_market_cap,
                        it.formatBigNumbers(applicationContext)
                    )
                )
            }

            peRatio?.let {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_pe_ratio,
                        it.format()
                    )
                )
            }

            earningsDate?.let {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_earnings_date,
                        it.formatDate(applicationContext.getString(R.string.date_format_long))
                    )
                )
            }

            dividendRate?.let {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_dividend_rate,
                        it.format()
                    )
                )
            }

            dividendDate?.let {
                details.add(
                    QuoteDetail(
                        R.string.quote_details_dividend_date,
                        it.formatDate(applicationContext.getString(R.string.date_format_long))
                    )
                )
            }

            emit(details)
        }
    }.flowOn(
        defaultDispatcher
    ).shareIn(
        viewModelScope,
        SharingStarted.WhileSubscribed(5000),
        1
    )

    fun fetchQuoteInRealTime(
        symbol: String? = _symbol.value,
        interval: Long? = _interval.value
    ) {
        fetchQuoteJob?.cancel()
        if (symbol != null && interval != null) {
            fetchQuoteJob = viewModelScope.launch {
                yahooFinanceRepository.getQuoteInRealTime(symbol, interval).collect {
                    _quote.value = it
                }
            }
        } else {
            _quote.value = Resource.Error()
        }
    }

    fun fetchChartInRealTime(
        symbol: String? = _symbol.value,
        chartPeriod: ChartPeriod = _chartPeriod.value,
        interval: Long = _interval.value,
    ) {
        fetchChartJob?.cancel()
        if (symbol != null) {
            fetchChartJob = viewModelScope.launch {
                yahooFinanceRepository.getChartInRealTime(symbol, chartPeriod, interval).collect {
                    _chart.value = it
                }
            }
        } else {
            _chart.value = Resource.Error()
        }
    }

    fun setSymbol(symbol: String) {
        _symbol.updateValue(symbol)
    }

    fun setPeriod(period: ChartPeriod) {
        _chartPeriod.updateValue(period)
    }

    fun setQuote(quote: Quote) {
        _cachedQuote.updateValue(quote)
    }

    fun toggleQuote() {
        viewModelScope.launch {
            if (isInWatchlist.latest == true) {
                _symbol.value?.let { stockRepository.deleteQuote(it) }
            } else {
                _cachedQuote.value?.let { stockRepository.insertQuote(it) }
            }
        }
    }

    companion object {
        private const val DEFAULT_INTERVAL: Long = 15 * 1000
        private val DEFAULT_PERIOD = ChartPeriod.DAY_1
    }
}
