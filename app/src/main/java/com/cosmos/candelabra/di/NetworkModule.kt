package com.cosmos.candelabra.di

import com.cosmos.candelabra.data.remote.api.autoc.AutocApi
import com.cosmos.candelabra.data.remote.api.yahoofinance.ChartPeriodConverterFactory
import com.cosmos.candelabra.data.remote.api.yahoofinance.CrumbInterceptor
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooConsent
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooCookieJar
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooFinance
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooFinanceApi
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooFinanceCrumb
import com.cosmos.candelabra.data.remote.api.yahoofinance.YahooInterceptor
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {

    @Retention(AnnotationRetention.RUNTIME)
    @Qualifier
    annotation class BasicOkHttp

    @Retention(AnnotationRetention.RUNTIME)
    @Qualifier
    annotation class YahooOkHttp

    @Retention(AnnotationRetention.RUNTIME)
    @Qualifier
    annotation class CrumbOkHttp

    @Provides
    @Singleton
    fun provideYahooMoshi(): Moshi {
        return Moshi.Builder().build()
    }

    @BasicOkHttp
    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }

    @YahooOkHttp
    @Provides
    @Singleton
    fun provideYahooOkHttpClient(
        crumbInterceptor: CrumbInterceptor,
        yahooInterceptor: YahooInterceptor,
        yahooCookieJar: YahooCookieJar
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(crumbInterceptor)
            .addInterceptor(yahooInterceptor)
            .cookieJar(yahooCookieJar)
            .build()
    }

    @CrumbOkHttp
    @Provides
    @Singleton
    fun provideCrumbOkHttpClient(
        yahooInterceptor: YahooInterceptor,
        yahooCookieJar: YahooCookieJar
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(yahooInterceptor)
            .cookieJar(yahooCookieJar)
            .build()
    }

    @Provides
    @Singleton
    fun provideYahooFinanceApi(
        moshi: Moshi,
        @YahooOkHttp okHttpClient: OkHttpClient
    ): YahooFinanceApi {
        return Retrofit.Builder()
            .baseUrl(YahooFinanceApi.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addConverterFactory(ChartPeriodConverterFactory())
            .client(okHttpClient)
            .build()
            .create(YahooFinanceApi::class.java)
    }

    @Provides
    @Singleton
    fun provideYahooFinanceCrumb(
        @CrumbOkHttp okHttpClient: OkHttpClient
    ): YahooFinanceCrumb {
        return Retrofit.Builder()
            .baseUrl(YahooFinanceCrumb.BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(YahooFinanceCrumb::class.java)
    }

    @Provides
    @Singleton
    fun provideYahooFinance(@CrumbOkHttp okHttpClient: OkHttpClient): YahooFinance {
        return Retrofit.Builder()
            .baseUrl(YahooFinance.BASE_URL)
            .client(okHttpClient)
            .build()
            .create(YahooFinance::class.java)
    }

    @Provides
    @Singleton
    fun provideYahooConsent(@CrumbOkHttp okHttpClient: OkHttpClient): YahooConsent {
        return Retrofit.Builder()
            .baseUrl(YahooConsent.BASE_URL)
            .client(okHttpClient)
            .build()
            .create(YahooConsent::class.java)
    }

    @Provides
    @Singleton
    fun provideAutocApi(moshi: Moshi, @BasicOkHttp okHttpClient: OkHttpClient): AutocApi {
        return Retrofit.Builder()
            .baseUrl(AutocApi.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .build()
            .create(AutocApi::class.java)
    }
}
